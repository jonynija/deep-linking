import express from 'express';
const Sentry = require('@sentry/node');
import { nodeProfilingIntegration } from '@sentry/profiling-node';
const handleProductRequest = require('./controllers/productController');

const app = express();
const port = 3000;

Sentry.init({
    dsn: 'https://7a543a78f7105e40084aad55266a3dcf@o4506367206948864.ingest.us.sentry.io/4507153679908864',
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Sentry.Integrations.Express({ app }),
        nodeProfilingIntegration(),
    ],
    // Performance Monitoring
    tracesSampleRate: 1.0, //  Capture 100% of the transactions
    // Set sampling rate for profiling - this is relative to tracesSampleRate
    profilesSampleRate: 1.0,
    environment: 'dev',
});

// The request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler());

// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

// Define a route for handling the deep link data
app.get('/product', handleProductRequest);

app.get('/debug-sentry', function mainHandler(req, res) {
    throw new Error('My first Sentry error!');
});

// The error handler must be registered before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err: any, req: any, res: any, next: any) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.write(JSON.stringify({ isCompleted: false, error: res.sentry }));
    res.end();
});

// Start the server
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

// http://192.168.0.1:3000/product
// localhost:3000/product
