import express from 'express';

const appStoreId = '1514667580';
const androidPackageName = 'com.agriconecta.agcta_smallholder_farmer';

function handleProductRequest(req: any, res: any) {
    // Extract the product ID and view identifier from the query parameters
    const productId = req.query.productId;
    const view = req.query.view || 'details'; // Default to the details view if no view parameter is specified

    // Generate the deep link URL with the product ID and view identifier included
    const deepLinkUrl = `agriconecta-app://product/${productId}?view=${view}`;

    // Check if the user has your app installed by using a user agent string
    const userAgent = req.headers['user-agent'] ?? '';
    console.log(userAgent);

    const isIos = /iPhone|iPad|iPod/.test(userAgent); // Check if the user is using an iOS device
    const isAndroid = /Android/.test(userAgent); // Check if the user is using an Android device

    console.log(`Is android ${isAndroid}, is iOS ${isIos} or is web ${!isAndroid && !isIos}`);

    if (isIos) {
        // Redirect the user to the deep link URL if they have an iOS device
        const iosUrl = `itms-apps://apple.com/app-id/${appStoreId}?action=view-pdp&url=${encodeURIComponent(
            deepLinkUrl
        )}`;
        res.redirect(iosUrl);
    } else if (isAndroid) {
        // Redirect the user to the deep link URL if they have an Android device
        const androidUrl = `market://details?id=${androidPackageName}&url=${encodeURIComponent(
            deepLinkUrl
        )}`;
        res.redirect(androidUrl);
    } else {
        // Redirect the user to the appropriate app store if they don't have your app installed
        /* res.redirect('https://agriconecta.com'); */
        throw new Error('No available for web platforms');
    }
}

module.exports = handleProductRequest;
