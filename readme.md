# Deep Link Handler for Product View

This is a Node.js application that serves as a deep link handler for a product view. It checks if the user has a custom app installed and redirects the user to the appropriate view within the app or to the app store for installation.

## Requirements

-   Node.js v12 or later
-   NPM (Node Package Manager)

## Setup

1. Clone the repository to your local machine.
2. Navigate to the root directory of the project.
3. Run `npm install` to install the dependencies.

## Usage

1. Start the server by running `npm start`.
2. Navigate to `http://localhost:3000/product?productId={PRODUCT_ID}&view={VIEW}` where `PRODUCT_ID` is the ID of the product to be viewed and `VIEW` is the optional view identifier. The server will generate a deep link URL with the product ID and view identifier included.
3. If the user has an iOS device, they will be redirected to the App Store to download the app or view the product within the app if already installed.
4. If the user has an Android device, they will be redirected to the Google Play Store to download the app or view the product within the app if already installed.
5. If the user doesn't have the app installed, they will be redirected to the website of the app.

## Dependencies

-   Express.js - A minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.
-   Node.js - A JavaScript runtime built on Chrome's V8 JavaScript engine.

## Configuration

The following variables need to be configured in the code before running the application:

-   `appStoreId`: The App Store ID for the app on iOS.
-   `androidPackageName`: The package name for the app on Android.

## License

This code is released under the MIT license. Feel free to modify and distribute it as you like.
